# 🪲 Ploot

## 🗿 Overview

**Ploot** is a web server project written in Rust, originally serving as a _[Proof of Concept (POC)](https://en.wikipedia.org/wiki/Proof_of_concept)_, but now a production-ready and complete web-server/framework. I plan to invest a significant amount of time into this project to ensure its quality. :)

Originally developed at: [GitHub/ploot](https://github.com/devkcud/ploot)
