pub mod builder;
pub mod status;

use std::{
    io::{BufRead, BufReader, Write},
    net::{TcpListener, TcpStream},
    thread,
};

fn main() {
    let listener = TcpListener::bind("localhost:3000").unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        thread::spawn(|| {
            handle_connection(stream);
        });
    }
}

fn handle_connection(mut stream: TcpStream) {
    let buf_reader = BufReader::new(&mut stream);
    let request_line = buf_reader.lines().next().unwrap().unwrap();

    let (code, content) = match &request_line[..] {
        "GET / HTTP/1.1" => (status::OK, "Hello, world!"),
        _ => (status::NOT_FOUND, ""),
    };

    let mut res = builder::Response::new();

    res.content(String::from(content));

    if code.code > 400 {
        res.error();
    }

    stream
        .write_all(res.build(status::NOT_FOUND).as_bytes())
        .unwrap();
}
