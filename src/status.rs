use std::u16;

#[derive(Debug)]
pub struct Code {
    pub desc: &'static str,
    pub code: u16,
}

// From: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#information_responses

pub const CONTINUE: Code = Code {
    desc: "100 Continue",
    code: 100,
};

pub const SWITCHING_PROTOCOLS: Code = Code {
    desc: "101 Switching Protocols",
    code: 101,
};

pub const PROCESSING: Code = Code {
    desc: "102 Processing",
    code: 102,
};

pub const EARLY_HINTS: Code = Code {
    desc: "103 Early Hints",
    code: 103,
};

pub const OK: Code = Code {
    desc: "200 OK",
    code: 200,
};

pub const CREATED: Code = Code {
    desc: "201 Created",
    code: 201,
};

pub const ACCEPTED: Code = Code {
    desc: "202 Accepted",
    code: 202,
};

pub const NON_AUTHORITATIVE_INFORMATION: Code = Code {
    desc: "203 Non-Authoritative Information",
    code: 203,
};

pub const NO_CONTENT: Code = Code {
    desc: "204 No Content",
    code: 204,
};

pub const RESET_CONTENT: Code = Code {
    desc: "205 Reset Content",
    code: 205,
};

pub const PARTIAL_CONTENT: Code = Code {
    desc: "206 Partial Content",
    code: 206,
};

pub const MULTI_STATUS: Code = Code {
    desc: "207 Multi-Status",
    code: 207,
};

pub const ALREADY_REPORTED: Code = Code {
    desc: "208 Already Reported",
    code: 208,
};

pub const IM_USED: Code = Code {
    desc: "226 IM Used",
    code: 226,
};

pub const MULTIPLE_CHOICES: Code = Code {
    desc: "300 Multiple Choices",
    code: 300,
};

pub const MOVED_PERMANENTLY: Code = Code {
    desc: "301 Moved Permanently",
    code: 301,
};

pub const FOUND: Code = Code {
    desc: "302 Found",
    code: 302,
};

pub const SEE_OTHER: Code = Code {
    desc: "303 See Other",
    code: 303,
};

pub const NOT_MODIFIED: Code = Code {
    desc: "304 Not Modified",
    code: 304,
};

pub const USE_PROXY: Code = Code {
    desc: "305 Use Proxy",
    code: 305,
};

pub const UNUSED: Code = Code {
    desc: "306 unused",
    code: 306,
};

pub const TEMPORARY_REDIRECT: Code = Code {
    desc: "307 Temporary Redirect",
    code: 307,
};

pub const PERMANENT_REDIRECT: Code = Code {
    desc: "308 Permanent Redirect",
    code: 308,
};

pub const BAD_REQUEST: Code = Code {
    desc: "400 Bad Request",
    code: 400,
};

pub const UNAUTHORIZED: Code = Code {
    desc: "401 Unauthorized",
    code: 401,
};

pub const PAYMENT_REQUIRED: Code = Code {
    desc: "402 Payment Required",
    code: 402,
};

pub const FORBIDDEN: Code = Code {
    desc: "403 Forbidden",
    code: 403,
};

pub const NOT_FOUND: Code = Code {
    desc: "404 Not Found",
    code: 404,
};

pub const METHOD_NOT_ALLOWED: Code = Code {
    desc: "405 Method Not Allowed",
    code: 405,
};

pub const NOT_ACCEPTABLE: Code = Code {
    desc: "406 Not Acceptable",
    code: 406,
};

pub const PROXY_AUTHENTICATION_REQUIRED: Code = Code {
    desc: "407 Proxy Authentication Required",
    code: 407,
};

pub const REQUEST_TIMEOUT: Code = Code {
    desc: "408 Request Timeout",
    code: 408,
};

pub const CONFLICT: Code = Code {
    desc: "409 Conflict",
    code: 409,
};

pub const GONE: Code = Code {
    desc: "410 Gone",
    code: 410,
};

pub const LENGTH_REQUIRED: Code = Code {
    desc: "411 Length Required",
    code: 411,
};

pub const PRECONDITION_FAILED: Code = Code {
    desc: "412 Precondition Failed",
    code: 412,
};

pub const PAYLOAD_TOO_LARGE: Code = Code {
    desc: "413 Payload Too Large",
    code: 413,
};

pub const URI_TOO_LONG: Code = Code {
    desc: "414 URI Too Long",
    code: 414,
};

pub const UNSUPPORTED_MEDIA_TYPE: Code = Code {
    desc: "415 Unsupported Media Type",
    code: 415,
};

pub const RANGE_NOT_SATISFIABLE: Code = Code {
    desc: "416 Range Not Satisfiable",
    code: 416,
};

pub const EXPECTATION_FAILED: Code = Code {
    desc: "417 Expectation Failed",
    code: 417,
};

pub const IM_A_TEAPOT: Code = Code {
    desc: "418 I'm a teapot",
    code: 418,
};

pub const MISDIRECTED_REQUEST: Code = Code {
    desc: "421 Misdirected Request",
    code: 421,
};

pub const UNPROCESSABLE_CONTENT: Code = Code {
    desc: "422 Unprocessable Content",
    code: 422,
};

pub const LOCKED: Code = Code {
    desc: "423 Locked",
    code: 423,
};

pub const FAILED_DEPENDENCY: Code = Code {
    desc: "424 Failed Dependency",
    code: 424,
};

pub const TOO_EARLY: Code = Code {
    desc: "425 Too Early",
    code: 425,
};

pub const UPGRADE_REQUIRED: Code = Code {
    desc: "426 Upgrade Required",
    code: 426,
};

pub const PRECONDITION_REQUIRED: Code = Code {
    desc: "428 Precondition Required",
    code: 428,
};

pub const TOO_MANY_REQUESTS: Code = Code {
    desc: "429 Too Many Requests",
    code: 429,
};

pub const REQUEST_HEADER_FIELDS_TOO_LARGE: Code = Code {
    desc: "431 Request Header Fields Too Large",
    code: 431,
};

pub const UNAVAILABLE_FOR_LEGAL_REASONS: Code = Code {
    desc: "451 Unavailable For Legal Reasons",
    code: 451,
};

pub const INTERNAL_SERVER_ERROR: Code = Code {
    desc: "500 Internal Server Error",
    code: 500,
};

pub const NOT_IMPLEMENTED: Code = Code {
    desc: "501 Not Implemented",
    code: 501,
};

pub const BAD_GATEWAY: Code = Code {
    desc: "502 Bad Gateway",
    code: 502,
};

pub const SERVICE_UNAVAILABLE: Code = Code {
    desc: "503 Service Unavailable",
    code: 503,
};

pub const GATEWAY_TIMEOUT: Code = Code {
    desc: "504 Gateway Timeout",
    code: 504,
};

pub const HTTP_VERSION_NOT_SUPPORTED: Code = Code {
    desc: "505 HTTP Version Not Supported",
    code: 505,
};

pub const VARIANT_ALSO_NEGOTIATES: Code = Code {
    desc: "506 Variant Also Negotiates",
    code: 506,
};

pub const INSUFFICIENT_STORAGE: Code = Code {
    desc: "507 Insufficient Storage",
    code: 507,
};

pub const LOOP_DETECTED: Code = Code {
    desc: "508 Loop Detected",
    code: 508,
};

pub const NOT_EXTENDED: Code = Code {
    desc: "510 Not Extended",
    code: 510,
};

pub const NETWORK_AUTHENTICATION_REQUIRED: Code = Code {
    desc: "511 Network Authentication Required",
    code: 511,
};

pub fn parse_code(code: u16) -> Option<Code> {
    match code {
        100 => Some(CONTINUE),
        101 => Some(SWITCHING_PROTOCOLS),
        102 => Some(PROCESSING),
        103 => Some(EARLY_HINTS),
        200 => Some(OK),
        201 => Some(CREATED),
        202 => Some(ACCEPTED),
        203 => Some(NON_AUTHORITATIVE_INFORMATION),
        204 => Some(NO_CONTENT),
        205 => Some(RESET_CONTENT),
        206 => Some(PARTIAL_CONTENT),
        207 => Some(MULTI_STATUS),
        208 => Some(ALREADY_REPORTED),
        226 => Some(IM_USED),
        300 => Some(MULTIPLE_CHOICES),
        301 => Some(MOVED_PERMANENTLY),
        302 => Some(FOUND),
        303 => Some(SEE_OTHER),
        304 => Some(NOT_MODIFIED),
        305 => Some(USE_PROXY),
        306 => Some(UNUSED),
        307 => Some(TEMPORARY_REDIRECT),
        308 => Some(PERMANENT_REDIRECT),
        400 => Some(BAD_REQUEST),
        401 => Some(UNAUTHORIZED),
        402 => Some(PAYMENT_REQUIRED),
        403 => Some(FORBIDDEN),
        404 => Some(NOT_FOUND),
        405 => Some(METHOD_NOT_ALLOWED),
        406 => Some(NOT_ACCEPTABLE),
        407 => Some(PROXY_AUTHENTICATION_REQUIRED),
        408 => Some(REQUEST_TIMEOUT),
        409 => Some(CONFLICT),
        410 => Some(GONE),
        411 => Some(LENGTH_REQUIRED),
        412 => Some(PRECONDITION_FAILED),
        413 => Some(PAYLOAD_TOO_LARGE),
        414 => Some(URI_TOO_LONG),
        415 => Some(UNSUPPORTED_MEDIA_TYPE),
        416 => Some(RANGE_NOT_SATISFIABLE),
        417 => Some(EXPECTATION_FAILED),
        418 => Some(IM_A_TEAPOT),
        421 => Some(MISDIRECTED_REQUEST),
        422 => Some(UNPROCESSABLE_CONTENT),
        423 => Some(LOCKED),
        424 => Some(FAILED_DEPENDENCY),
        425 => Some(TOO_EARLY),
        426 => Some(UPGRADE_REQUIRED),
        428 => Some(PRECONDITION_REQUIRED),
        429 => Some(TOO_MANY_REQUESTS),
        431 => Some(REQUEST_HEADER_FIELDS_TOO_LARGE),
        451 => Some(UNAVAILABLE_FOR_LEGAL_REASONS),
        500 => Some(INTERNAL_SERVER_ERROR),
        501 => Some(NOT_IMPLEMENTED),
        502 => Some(BAD_GATEWAY),
        503 => Some(SERVICE_UNAVAILABLE),
        504 => Some(GATEWAY_TIMEOUT),
        505 => Some(HTTP_VERSION_NOT_SUPPORTED),
        506 => Some(VARIANT_ALSO_NEGOTIATES),
        507 => Some(INSUFFICIENT_STORAGE),
        508 => Some(LOOP_DETECTED),
        510 => Some(NOT_EXTENDED),
        511 => Some(NETWORK_AUTHENTICATION_REQUIRED),
        _ => None,
    }
}
