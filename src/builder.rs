use crate::status;

pub struct Response {
    content: String,
    content_type: String,
}

impl Response {
    pub fn new() -> Self {
        Self {
            content: String::new(),
            content_type: String::from("plain/text"),
        }
    }

    pub fn text(&mut self) -> &mut Self {
        self.content_type = String::from("plain/text");
        self
    }

    pub fn json(&mut self) -> &mut Self {
        self.content_type = String::from("application/json");
        self
    }

    pub fn error(&mut self) -> &mut Self {
        let slashed = self.content_type.splitn(2, '/').collect::<Vec<&str>>();
        println!("{:#?}", slashed);

        self.content_type = format!(
            "{}/error+{}",
            slashed.get(0).unwrap(),
            slashed.get(1).unwrap(),
        );
        self
    }

    pub fn content(&mut self, content: String) -> &mut Self {
        self.content = content;
        self
    }

    pub fn build(&self, status_code: status::Code) -> String {
        let status_line = format!("HTTP/1.1 {}", status_code.desc);
        let content_type = &self.content_type;
        let contents = &self.content;
        let length = self.content.len();

        format!("{status_line}\r\nContent-Type: {content_type}\r\nContent-Length: {length}\r\n\r\n{contents}")
    }
}
